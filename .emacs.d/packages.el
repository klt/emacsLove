;;; Package Manager

(require 'package)
(package-initialize)

(setq use-package-always-ensure t)

(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
;; Marmalade is not tasty anymore
;;        ("marmalade" . "https://marmalade-repo.org/packages/")
        ("melpa-stable" . "http://stable.melpa.org/packages/")
        ("org" . "http://orgmode.org/elpa/")
        )
      )

(when (not package-archive-contents)
   (package-refresh-contents)
   (package-install 'use-package))

;;(require 'use-package)

(defun ensure-package-installed (&rest packages)
  "Assure every package is installed, ask for installation if it’s not.

Return a list of installed packages or nil for every skipped package."
  (mapcar
   (lambda (package)
     ;; (package-installed-p 'evil)
     (if (package-installed-p package)
         nil
       (if (y-or-n-p (format "Package %s is missing. Install it? " package))
           (package-install package)
         package)))
   packages))

;; make sure to have downloaded archive description.
;; Or use package-archive-contents as suggested by Nicolas Dudebout
(or (file-exists-p package-user-dir)
    (package-refresh-contents))

(setq packages-must-have
      '(auctex company csv-mode dash json-mode json-reformat
               osx-dictionary popup s svg use-package bind-key yasnippet))

(apply #'ensure-package-installed packages-must-have)



;; activate installed packages
(package-initialize)

(message "packages.el loaded :)")

;;; Local Variables:
;;; mode: emacs-lisp
;;; coding: utf-8
;;; End:
