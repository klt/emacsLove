; Shell script mode

(set-face-attribute 'sh-heredoc
                    nil
                    :foreground  "blue"
                    :slant 'normal
                    :weight 'normal)

;;; Local Variables: 
;;; mode: emacs-lisp
;;; coding: utf-8
;;; End:
